# -*- coding: utf-8 -*-
"""
Created: Friday, June 04 2024
Description: script to draw a geo-info map using basemap lib
Scope: for the supply chain resilience project
Author: Quanliang Ye
Institution: Aalborg University, Aalborg, Denmark; Nijmegen School of Management, Radboud University, Nijmegen, Netherlands
Email: yequanliang1993@gmail.com
"""

from pathlib import Path

import fiona
import geopandas as gpd
import matplotlib.pyplot as plt
import pandas as pd
from matplotlib.patches import Polygon
from mpl_toolkits.basemap import Basemap

home_path = Path.home() / "OneDrive - Aalborg Universitet"
project_root_path = (
    home_path / "Research ALL PROJECTS/(0 Processing) Supply chain resilience/data"
)

# ----------------------------------------
# CREATE A BASEMAP
# ----------------------------------------

# Create a new figure
# plt.figure(figsize=(4, 4))


map = Basemap(
    projection="merc",
    lat_0=36,
    lon_0=104,
    llcrnrlat=18.0,
    urcrnrlat=54.0,  # Latitude range for China
    llcrnrlon=73.0,
    urcrnrlon=135.0,  # Longitude range for China
    resolution="l",
)


# ----------------------------------------
# READ SHAPE FILE OF PROVINCES IN CHINA
# ----------------------------------------
chn_shp_path = (
    home_path
    / "Research ALL PROJECTS/(2 None) Data China/china_boundaries/china_province"
)
chn_shp_name = "china_province"
map.readshapefile(
    chn_shp_path,
    chn_shp_name,
    drawbounds=True,
)

# ----------------------------------------
# DATA IMPORT
# ----------------------------------------
pro_groups_path = project_root_path / "province_groups.csv"
pro_groups = pd.read_csv(pro_groups_path)
pro_groups = pro_groups.set_index("province")

# ----------------------------------------
# GRO-INFO
# ----------------------------------------
fig = plt.gca()


# using patch function
def pcolor(
    seg,
    group_name: str,
):
    """
    This is a patch function to color each patch based on the parameter values

    Parameter
    ---------
    seg
      The patch

    group_name
        In which group the province belongs to

    """

    colors = ""
    if group_name == "a":
        colors = "#206061"  # hardcoding color codes
        label_ = "Group A"  # labels for legend
    elif group_name == "b":
        colors = "#fbcdf9"
        label_ = "Group B"
    elif group_name == "c":
        colors = "#f19e6a"
        label_ = "Group C"
    elif group_name == "d":
        colors = "#818332"
        label_ = "Group D"
    else:
        colors = "grey"
        label_ = "No Data"

    poly = Polygon(
        seg,
        facecolor=colors,
        alpha=0.4,
        label=label_,
    )
    fig.add_patch(poly)


for seg, province_info in zip(map.china_province, map.china_province_info):
    province_name = province_info["FENAME"]

    province_name_ = province_name.split(" ")[0]
    if province_name_ == "Neimongol":
        province_name_ = "Inner Mongolia"
    elif province_name_ == "Xinjianguygur":
        province_name_ = "Xinjiang"
    elif province_name_ == "SHanxi":
        province_name_ = "Shaanxi"
    elif province_name_ == "Ningxiahuizu":
        province_name_ = "Ningxia"
    elif province_name_ == "Xizang":
        province_name_ = "Tibet"
    elif province_name_ == "Guangxizhuangzu":
        province_name_ = "Guangxi"

    try:
        group_name = pro_groups.loc[province_name_, "group_name"]
    except:
        group_name = None

    pcolor(seg, group_name)

# ----------------------------------------
# SET LEGEND
# ----------------------------------------
handles, labels = fig.get_legend_handles_labels()
fontsize = 12

# legend for groups
leg_handles = []
leg_labels = []
for share_range in [
    "Group A",
    "Group B",
    "Group C",
    "Group D",
    "No Data",
]:
    index = labels.index(share_range)
    leg_handles.append(handles[index])
    leg_labels.append(labels[index])
legend1 = plt.legend(
    leg_handles,
    leg_labels,
    edgecolor=None,
    loc=(0.4, 0.65),
    title="Groups",
    title_fontsize=fontsize,
    alignment="left",
    fontsize=fontsize,
)
frame = legend1.get_frame()
frame.set_facecolor("none")
frame.set_edgecolor("none")
fig.add_artist(legend1)

# ----------------------------------------
# SAVE FIGURE
# ----------------------------------------
export_file_name = "figure_province_groups.png"
plt.tight_layout()
plt.savefig(
    project_root_path / export_file_name,
    dpi=600,
    facecolor="none",
)

# plt.show()
