# -*- coding: utf-8 -*-
"""
Created: Sat, June 29 2024
Description: script to draw a geo-info map using basemap lib
Scope: for capital equity project
Author: Quanliang Ye
Institution: Aalborg University, Aalborg, Denmark; Nijmegen School of Management, Radboud University, Nijmegen, Netherlands
Email: yequanliang1993@gmail.com
"""

from pathlib import Path

import fiona
import geopandas as gpd
import matplotlib.pyplot as plt
import pandas as pd
import shapefile
from matplotlib.collections import PatchCollection
from matplotlib.patches import Polygon
from mpl_toolkits.basemap import Basemap
import matplotlib.cm as cm
import matplotlib.colors as colors
import numpy as np

home_path = Path.home() / "OneDrive - Aalborg Universitet"
project_root_path = (
    home_path
    / "Research ALL PROJECTS/(0 Processing) Equity across generation as commentary/Data"
)


# ----------------------------------------
# CREATE A BASEMAP
# ----------------------------------------

for capital_column in [
    "Human capital per capita ",
    "Natural capital per capita ",
    "Produced capital per capita ",
]:
    # figure, ax = plt.figure()
    fig, ax = plt.subplots()

    # Create a Basemap instance
    map = Basemap(
        projection="mill",
        llcrnrlat=-60,
        urcrnrlat=90,
        llcrnrlon=-180,
        urcrnrlon=180,
        resolution="c",
    )

    # ----------------------------------------
    # READ SHAPE FILE OF PROVINCES IN CHINA
    # ----------------------------------------
    world_shp_path = (
        project_root_path / "world_shapefile/world_administrative_boundaries"
    )
    world_shp_name = "world_administrative_boundaries"
    map.readshapefile(
        world_shp_path,
        world_shp_name,
        drawbounds=True,
    )

    # ----------------------------------------
    # DATA IMPORT
    # ----------------------------------------
    change_capital_path = (
        project_root_path / "wealth_data/change_in_wealth_by_asset_region.csv"
    )
    change_capital = pd.read_csv(change_capital_path)
    change_capital = change_capital.set_index("Country Code")

    # ----------------------------------------
    # GRO-INFO
    # ----------------------------------------
    fig = plt.gca()

    # using patch function
    def pcolor(
        seg,
        change_capital_index: int,
    ):
        """
        This is a patch function to color each patch based on the parameter values

        Parameter
        ---------
        seg
        The patch

        change_capital_index
            Index to determine the color

        """
        if change_capital_index:
            poly = Polygon(
                seg,
                facecolor=cmap(norm(change_capital_index)),
                # alpha=0.8,
                # label=label_,
            )

        else:
            poly = Polygon(
                seg,
                facecolor="white",
                # alpha=0.8,
                # label=label_,
            )
        fig.add_patch(poly)

    if capital_column == "Human capital per capita ":
        # Create a colormap with 6 colors
        num_colors = 5
        cmap = cm.get_cmap("Oranges", num_colors)  # Choose a colormap
        norm = colors.Normalize(vmin=0, vmax=num_colors)

        for seg, country_change in zip(
            map.world_administrative_boundaries,
            map.world_administrative_boundaries_info,
        ):

            country_name = country_change["color_code"]

            try:
                change_capital_ = change_capital.loc[country_name, capital_column]
                if change_capital_ < 0:
                    change_capital_index = 1
                elif change_capital_ < 100:
                    change_capital_index = 2
                elif change_capital_ < 200:
                    change_capital_index = 3
                elif change_capital_ < 300:
                    change_capital_index = 4
                else:
                    change_capital_index = 5
            except:
                change_capital_index = None

            pcolor(seg, change_capital_index)
    elif capital_column == "Natural capital per capita ":
        # Create a colormap with 6 colors
        num_colors = 5
        cmap = cm.get_cmap("Greens", num_colors)  # Choose a colormap
        norm = colors.Normalize(vmin=0, vmax=num_colors)

        for seg, country_change in zip(
            map.world_administrative_boundaries,
            map.world_administrative_boundaries_info,
        ):

            country_name = country_change["color_code"]

            try:
                change_capital_ = change_capital.loc[country_name, capital_column]
                if change_capital_ < -50:
                    change_capital_index = 1
                elif change_capital_ < 0:
                    change_capital_index = 2
                elif change_capital_ < 50:
                    change_capital_index = 3
                elif change_capital_ < 100:
                    change_capital_index = 4
                else:
                    change_capital_index = 5
            except:
                change_capital_index = None

            pcolor(seg, change_capital_index)
    else:
        # Create a colormap with 6 colors
        num_colors = 5
        cmap = cm.get_cmap("Purples", num_colors)  # Choose a colormap
        norm = colors.Normalize(vmin=0, vmax=num_colors)

        for seg, country_change in zip(
            map.world_administrative_boundaries,
            map.world_administrative_boundaries_info,
        ):

            country_name = country_change["color_code"]

            try:
                change_capital_ = change_capital.loc[country_name, capital_column]
                if change_capital_ < 0:
                    change_capital_index = 1
                elif change_capital_ < 100:
                    change_capital_index = 2
                elif change_capital_ < 200:
                    change_capital_index = 3
                elif change_capital_ < 300:
                    change_capital_index = 4
                else:
                    change_capital_index = 5
            except:
                change_capital_index = None

            pcolor(seg, change_capital_index)
    # ----------------------------------------
    # SET LEGEND
    # ----------------------------------------
    handles, labels = fig.get_legend_handles_labels()
    # plt.title(f"Changes in {capital_column[:-1].lower()}")
    fontsize = 7

    # legend for groups
    cbar = plt.colorbar(
        cm.ScalarMappable(norm=norm, cmap=cmap),
        ticks=np.arange(num_colors),
        orientation="horizontal",
    )
    cbar.set_label("%")
    # cbar.ax.tick_params(labelsize=fontsize)

    # ----------------------------------------
    # SAVE FIGURE
    # ----------------------------------------

    export_file_name = f"figure_{capital_column}.tif"
    plt.tight_layout()
    plt.savefig(
        project_root_path / export_file_name,
        dpi=600,
        facecolor="none",
    )

    plt.close()
